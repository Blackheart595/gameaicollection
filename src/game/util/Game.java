package game.util;

public interface Game<S extends State<S, ?, ?, ?>> {
    S getInitialState();
}
