package game.util;

public interface FullInformationView<S extends FullInformationState<S, V, M, P>, V extends FullInformationView<S, V, M, P>, M extends Move, P extends Player> extends View<M, P> {
    S getState();
}
