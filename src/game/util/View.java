package game.util;

import java.util.Set;

public interface View<M extends Move, P extends Player> {
    boolean hasEnded();
    double getScore();
    Set<M> getPossibleMoves();
    boolean isMovePossible(M move);
    P getViewer();
}
