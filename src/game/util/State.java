package game.util;

import game.util.random.RandomChoice;

import java.util.Map;
import java.util.Set;

public interface State<S extends State<S, V, M, P>, V extends View<M, P>, M extends Move, P extends Player> {
    boolean hasEnded();
    Map<P, Double> getScore();
    double getScore(P player);
    RandomChoice<M> getRandomChoice();
    Set<P> getActivePlayers();
    Map<P, Set<M>> getPossibleMoves();
    Set<M> getPossibleMoves(P player);
    void applyMoves(Map<P, M> moves);
    void applyRandomMove(M move);
    boolean isMovePossible(P player, M move);
    boolean isRandomMovePossible(M move);
    V getView(P player);
    S getCopy();
}
