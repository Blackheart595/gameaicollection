package game.util;

public interface FullInformationState<S extends FullInformationState<S, V, M, P>, V extends FullInformationView<S, V, M, P>,  M extends Move, P extends Player> extends State<S, V, M, P> {
}
