package game.util.random;

import game.util.Move;

import java.math.BigInteger;
import java.util.function.Function;

final class RandomBigBoundChoice<M extends Move> extends RandomChoice<M> {
    private final BigInteger bound;
    private final Function<BigInteger, M> moves;

    RandomBigBoundChoice(BigInteger bound, Function<BigInteger, M> moves) {
        this.bound = bound;
        this.moves = moves;
    }

    @Override
    public M askRandom(Random random) {
        return random.chooseMove(bound, moves);
    }
}
