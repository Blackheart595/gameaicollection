package game.util.random;

import game.util.Move;

import java.util.function.IntFunction;

final class RandomWeightChoice<M extends Move> extends RandomChoice<M> {
    private final int sum;
    private final int[] weights;
    private final IntFunction<M> moves;

    RandomWeightChoice(int sum, int[] weights, IntFunction<M> moves) {
        this.sum = sum;
        this.weights = weights;
        this.moves = moves;
    }

    @Override
    public M askRandom(Random random) {
        return random.chooseMove(sum, weights, moves);
    }
}
