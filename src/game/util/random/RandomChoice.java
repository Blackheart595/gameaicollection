package game.util.random;

import game.util.Move;

import java.math.BigInteger;
import java.util.function.Function;
import java.util.function.IntFunction;

public abstract class RandomChoice<M extends Move> {
    public static <M extends Move> RandomChoice<M> fromProbability(double[] probabilities, IntFunction<M> moves) {
        return new RandomProbabilityChoice<>(probabilities, moves);
    }
    public static <M extends Move> RandomChoice<M> fromWeights(int sum, int[] weights, IntFunction<M> moves) {
        return new RandomWeightChoice<>(sum, weights, moves);
    }
    public static <M extends Move> RandomChoice<M> fromBound(int bound, IntFunction<M> moves) {
        return new RandomBoundChoice<>(bound, moves);
    }
    public static <M extends Move> RandomChoice<M> fromBigWeights(BigInteger sum, BigInteger[] weights, IntFunction<M> moves) {
        return new RandomBigWeightChoice<>(sum, weights, moves);
    }
    public static <M extends Move> RandomChoice<M> fromBigBound(BigInteger bound, Function<BigInteger, M> moves) {
        return new RandomBigBoundChoice<>(bound, moves);
    }

    RandomChoice() {}

    public abstract M askRandom(Random random);
}
