package game.util.random;

import game.util.Move;

import java.util.function.IntFunction;

final class RandomProbabilityChoice<M extends Move> extends RandomChoice<M> {
    private final double[] probabilities;
    private final IntFunction<M> moves;

    RandomProbabilityChoice(double[] probabilities, IntFunction<M> moves) {
        this.probabilities = probabilities;
        this.moves = moves;
    }

    @Override
    public M askRandom(Random random) {
        return random.chooseMove(probabilities, moves);
    }
}
