package game.util.random;

import game.util.Move;

import java.math.BigInteger;
import java.util.function.Function;
import java.util.function.IntFunction;

public interface Random {
    <M extends Move> M chooseMove(double[] probabilities, IntFunction<M> moves);
    <M extends Move> M chooseMove(int sum, int[] weights, IntFunction<M> moves);
    <M extends Move> M chooseMove(int bound, IntFunction<M> moves);
    <M extends Move> M chooseMove(BigInteger sum, BigInteger[] weights, IntFunction<M> moves);
    <M extends Move> M chooseMove(BigInteger bound, Function<BigInteger, M> moves);
}
