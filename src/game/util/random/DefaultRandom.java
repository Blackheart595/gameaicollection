package game.util.random;

import game.util.Move;

import java.math.BigInteger;
import java.util.function.Function;
import java.util.function.IntFunction;

public final class DefaultRandom implements Random {
    private final java.util.Random random;

    public DefaultRandom() {
        random = new java.util.Random();
    }

    public DefaultRandom(long seed) {
        random = new java.util.Random(seed);
    }

    @Override
    public <M extends Move> M chooseMove(double[] probabilities, IntFunction<M> moves) {
        int length = probabilities.length;
        double rand = random.nextDouble();
        double r = rand;
        for (int i = 0; i < length; i++) {
            if ((r -= probabilities[i]) < 0) {
                return moves.apply(i);
            }
        }
        return moves.apply((int) (length * rand));
    }

    @Override
    public <M extends Move> M chooseMove(int sum, int[] weights, IntFunction<M> moves) {
        int length = weights.length;
        int r = random.nextInt(sum);
        for (int i = 0; i < length; i++) {
            if ((r -= weights[i]) < 0) {
                return moves.apply(i);
            }
        }
        return null;
    }

    @Override
    public <M extends Move> M chooseMove(int bound, IntFunction<M> moves) {
        return moves.apply(random.nextInt(bound));
    }

    @Override
    public <M extends Move> M chooseMove(BigInteger sum, BigInteger[] weights, IntFunction<M> moves) {
        int length = weights.length;
        BigInteger r = randomBigInteger(sum);
        for (int i = 0; i < length; i++) {
            if ((r = r.subtract(weights[i])).signum() < 0) {
                return moves.apply(i);
            }
        }
        return null;
    }

    @Override
    public <M extends Move> M chooseMove(BigInteger bound, Function<BigInteger, M> moves) {
        return moves.apply(randomBigInteger(bound));
    }

    private BigInteger randomBigInteger(BigInteger bound) {
        BigInteger m = bound.subtract(BigInteger.ONE);
        int bitLength = m.bitLength();
        if (bound.bitLength() > bitLength) { // i.e., bound is a power of 2
            return new BigInteger(bitLength, random);
        } else {
            BigInteger u = new BigInteger(bitLength <<= 1, random);
            BigInteger r = u.remainder(bound);
            while (u.subtract(r).add(m).signum() < 0) {
                u = new BigInteger(bitLength, random);
                r = u.remainder(bound);
            }
            return r;
        }
    }
}
