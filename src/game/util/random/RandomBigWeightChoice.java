package game.util.random;

import game.util.Move;

import java.math.BigInteger;
import java.util.function.IntFunction;

final class RandomBigWeightChoice<M extends Move> extends RandomChoice<M> {
    private final BigInteger sum;
    private final BigInteger[] weights;
    private final IntFunction<M> moves;

    RandomBigWeightChoice(BigInteger sum, BigInteger[] weights, IntFunction<M> moves) {
        this.sum = sum;
        this.weights = weights;
        this.moves = moves;
    }

    @Override
    public M askRandom(Random random) {
        return random.chooseMove(sum, weights, moves);
    }
}
