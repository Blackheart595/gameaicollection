package game.util.random;

import game.util.Move;

import java.util.function.IntFunction;

final class RandomBoundChoice<M extends Move> extends RandomChoice<M> {
    private final int bound;
    private final IntFunction<M> moves;

    RandomBoundChoice(int bound, IntFunction<M> moves) {
        this.bound = bound;
        this.moves = moves;
    }

    @Override
    public M askRandom(Random random) {
        return random.chooseMove(bound, moves);
    }
}
